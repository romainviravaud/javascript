const { MongoClient } = require("mongodb");

const client = new MongoClient("mongodb://localhost:27017");

client
  .connect()
  .then(function () {
    console.log("Conectado!!");

    // EXERCICE 4----
    //-------------------------------------

    client
      .db("ecommerce")
      .collection("pedidos")
      .find({ usuario: "cristiano" })
      .forEach(function (resultat) {
        let ordre = resultat.producto.sort((a, b) => b - a);
        console.log(ordre);
      })

      .then(function () {
        console.log("find correcto");
      })

      .catch(function (error) {
        console.log(error);
      });
  })

  //EXERCICE 5 -----------------------------
  .catch(function () {
    console.log("Error, no se ha podido conectar");
  });
