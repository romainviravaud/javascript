const { MongoClient } = require("mongodb");

const client = new MongoClient("mongodb://localhost:27017");

client
  .connect()
  .then(function () {
    console.log("Conectado!!");

    client
      .db("ecommerce")
      .collection("usuarios")
      .find({ nombre: "lucette" })
      .forEach(function (resultat) {
        console.log(resultat.nombre, resultat.apellido);
      })
      .then(function () {
        console.log("find correcto");
      })
      .catch(function () {
        console.log("Error en el find");
      });
  })
  .catch(function () {
    console.log("Error, no se ha podido conectar");
  });
