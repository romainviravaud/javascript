let nombreEtoiles = 7;

function imprimerLigne(longueur, file) {
  let ligne = "";
  for (let i = 0; i < longueur; i++) {
    if (i < file) {
      ligne += " ";
    } else {
      ligne += "*";
    }
  }

  return ligne;
}
for (let i = 0; i < nombreEtoiles; i++) {
  console.log(imprimerLigne(nombreEtoiles, i));
}
