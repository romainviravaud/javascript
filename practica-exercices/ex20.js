/**
 * Requisitos:
 *  Password es "eureka"
 *  tengo 3 intentos
 *
 * 1. Pedir password
 * 2. Comprobar password
 * 3. Si password correcto -> mostrar mensaje y salir
 * 4. Si password no correcto ->
 *  intentos = intentos - 1
 *  Si tengo intentos voy al paso 1
 *  No tengo intentos -> mostrar mensaje y salir
 */

const codeCorrect = "eureka";
let essais = 3;
let messageAMontrer = "Password incorrecta";

// 1. Pedir password con prompt (de momento lo hago solo constante)
const codeDemandé = "eureka";

// 2. Comprobar password
function verifierCode(password) {
  return password === codeCorrect;
}

// 3. Si password correcta -> mostrar mensaje y salir
do {
  // TODO: Incluir prompt
  if (verifierCode(codeDemandé)) {
    messageAMontrer = "Password correcta";

    essais = 0;
  }
} while (--essais > 0);

console.log(messageAMontrer);
