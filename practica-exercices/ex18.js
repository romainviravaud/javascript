/**
 * 1. Pedir el valor de n
 * 2. Comprobar si es par
 *  Si es par suma 2
 *  Si es impar suma 1
 * 3. Guardar en variable suma el valor de s
 * 4. s = s + 2
 * 5. Suma = suma + s
 * 6. Volver al 4 (n - 1) veces
 /

// TODO: pedir n por prompt
const n = 3;

function esPar(numero) {
    return numero % 2 == 0;
}

// 2. Comprobar si es par
function convertirAPar(numero) {
    /if (!esPar(numero)) {
        return numero + 1;
    } else {
        return numero + 2;
    }*/
return !esPar(numero) ? numero + 1 : numero + 2;

//let s = n + convertirAPar(n);
let s = n + (2 - (n % 2));

// 3. Guardar en suma el valor de s
let suma = s;

for (let i = 0; i < n - 1; i++) {
  s += 2;
  suma += s;
}

console.log(suma);
